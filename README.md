# README #


### This repository is for ###

* Sniper Training game.
* Version 1.0

# Pre-requirtes #

* Java 1.8
* Maven 3

### Set up Instructions ###

* Clone this repo https://gitlab.com/anilkumardevgan/sniper-training.git on your local machine.
* Navigate to project root directory and run this command "mvn clean package".
* All the required dependencies are downloaded and project is build with the above command.
* No Database required to play this game.
* After you receive Build Success message, navigate to the target folder, there will be jar named sniper-training-1.0.jar

### Running jar from terminal with following command ###

java -cp sniper-training-1.0.jar com.training.sniper.SniperTrainingApplication

SniperTrainingApplication.java is the entry point of this sniper training game which contains main method.
