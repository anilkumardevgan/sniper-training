package com.training.sniper.game.utils;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.EnumSet;

import org.junit.Test;

import com.training.sniper.game.enums.Enums.DamageCapability;

public class RandomEnumSelectorTests {

	@Test
	public void testRandomEnum() {
		DamageCapability damageCapability = RandomEnumSelector.randomEnum(EnumSet.allOf(DamageCapability.class));
		assertThat(damageCapability, instanceOf(DamageCapability.class));
	}
}
