package com.training.sniper.game.utils;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class OptionsItemsLimitsTests {

	@Test(expected = IllegalStateException.class)
	public void testInvalidRange() {
		OptionsItemsLimits.of(8, 6);
	}

	@Test
	public void testRangeObj() {
		OptionsItemsLimits obj = OptionsItemsLimits.of(3, 6);
		assertThat(obj, instanceOf(OptionsItemsLimits.class));
	}

	@Test
	public void testRangeTrue() {
		OptionsItemsLimits obj = OptionsItemsLimits.of(3, 6);
		assertThat(obj.contains(5), is(true));
	}

	@Test
	public void testRangeMaxFalse() {
		OptionsItemsLimits obj = OptionsItemsLimits.of(3, 6);
		assertThat(obj.contains(10), is(false));
	}

	@Test
	public void testRangeMinFalse() {
		OptionsItemsLimits obj = OptionsItemsLimits.of(3, 6);
		assertThat(obj.contains(1), is(false));
	}

}
