package com.training.sniper.game.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;

import org.junit.Test;

public class TerminalValueReaderTests {

    @Test
    public void testReadValue() {
        ByteArrayInputStream in = new ByteArrayInputStream("Anil".getBytes());
        System.setIn(in);
        String val = TerminalValueReader.INSTANCE.readString();
        System.setIn(System.in);
        assertThat(val, is("Anil"));
    }

    @Test
    public void testInstance() {
    	TerminalValueReader vReader = TerminalValueReader.INSTANCE;
        assertThat(vReader, is(TerminalValueReader.INSTANCE));
    }
    
}
