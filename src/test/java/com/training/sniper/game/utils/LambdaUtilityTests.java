package com.training.sniper.game.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

import com.training.sniper.game.enums.Enums.TrainingStatus;

public class LambdaUtilityTests {

	@Test
	public void testEnumValToStaringValL() {
		List<String> values = new ArrayList<>();
		Stream.of(TrainingStatus.values()).map(LambdaUtility.ENUMVAL_TO_STRINGVAL).forEach(e -> values.add(e));
		assertThat(values.size(), is(3));
	}

	@Test
	public void testValuePositiveTrue() {
		assertThat(LambdaUtility.VALUE_POSITIVE.test("25"), is(true));
	}

	@Test
	public void testValuePositiveFalse() {
		assertThat(LambdaUtility.VALUE_POSITIVE.test("-86"), is(false));
	}

	@Test
	public void testNumValFalse() {
		assertThat(LambdaUtility.VALUE_IS_NUM.test("test"), is(false));
	}

	@Test
	public void testNumValNullFalse() {
		assertThat(LambdaUtility.VALUE_IS_NUM.test(null), is(false));
	}

	@Test
	public void testNumValTrue() {
		assertThat(LambdaUtility.VALUE_IS_NUM.test("567"), is(true));
	}

	@Test
	public void testValueNotEmptyTrue() {
		assertThat(LambdaUtility.VALUE_NOT_EMPTY.test("346"), is(true));
	}

	@Test
	public void testValueNotEmptyFalse() {
		assertThat(LambdaUtility.VALUE_NOT_EMPTY.test(""), is(false));
	}

	@Test
	public void testValueNotEmptyNullFalse() {
		assertThat(LambdaUtility.VALUE_NOT_EMPTY.test(null), is(false));
	}

	@Test
	public void testValueNotEmptySpaceFalse() {
		assertThat(LambdaUtility.VALUE_NOT_EMPTY.test("    "), is(false));
	}

	@Test
	public void testCorrectValueTrue() {
		assertThat(LambdaUtility.CORRECT_VALUE.test("965"), is(true));
	}

	@Test
	public void testCorrectValueFalse() {
		assertThat(LambdaUtility.CORRECT_VALUE.test("-753"), is(false));
	}

}
