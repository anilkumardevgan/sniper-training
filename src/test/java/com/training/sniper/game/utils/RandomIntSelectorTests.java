package com.training.sniper.game.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class RandomIntSelectorTests {
	@Test
	public void testGetRandomIntFromRange() {
		int val = RandomIntSelector.getRandomIntFromRange(15, 25);
		assertTrue(val >= 15);
		assertTrue(val <= 25);
	}
}
