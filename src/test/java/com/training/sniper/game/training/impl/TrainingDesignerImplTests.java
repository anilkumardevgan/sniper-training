package com.training.sniper.game.training.impl;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import com.training.sniper.game.builders.TerminalOutput;
import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.training.TrainingTerminalAction;
import com.training.sniper.game.training.TrainingAlertAction;
import com.training.sniper.game.training.observer.ObserverImpl;
import com.training.sniper.game.training.strategy.TrainingStrategyImpl;
import com.training.sniper.game.utils.SniperCreator;

public class TrainingDesignerImplTests extends TerminalOutput{
private StartOptions<TrainingAction> options = new StartOptions<>("Choose your action:", TrainingAction.values()); 

    @Test
    public void testStartShouldShowGamePausedNotification() throws Exception {
        Sniper opponent = SniperCreator.buildOpponent().get(0);
        Sniper sniper = SniperCreator.buildSniper();
        ObserverImpl observerImpl = mock(ObserverImpl.class);
        doNothing().when(observerImpl).begin();
        when(observerImpl.getTrainingStatus()).thenReturn(TrainingStatus.SAVED);
        when(observerImpl.getSniper()).thenReturn(sniper);
        when(observerImpl.getOpponent()).thenReturn(opponent);
        PowerMockito.whenNew(ObserverImpl.class).withArguments(opponent,sniper,new TrainingStrategyImpl(new TrainingTerminalAction(options))).thenReturn(observerImpl);
        TrainingDesignerImpl designerImpl = new TrainingDesignerImpl(observerImpl, new TrainingAlertAction());
        designerImpl.begin();
        String actual = terminalVal.toString();
        assertThat(actual , containsString("Training has been paused"));
    }
}
