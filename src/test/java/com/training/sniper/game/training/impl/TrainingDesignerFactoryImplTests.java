package com.training.sniper.game.training.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.training.TrainingTerminalAction;
import com.training.sniper.game.training.TrainingAlertAction;
import com.training.sniper.game.training.TrainingDesigner;
import com.training.sniper.game.training.observer.ObserverImpl;
import com.training.sniper.game.training.strategy.TrainingStrategyImpl;
import com.training.sniper.game.utils.SniperCreator;

public class TrainingDesignerFactoryImplTests {


    private ObserverImpl observerImpl;

    private TrainingInitializerImpl trainingInitializerImpl;
    
    private Sniper opponent;
    
    private Sniper sniper;

    @Before
    public void initialize() {
        StartOptions<TrainingAction> options = new StartOptions<>("Choose your action:", TrainingAction.values());
        opponent = SniperCreator.buildOpponent().get(0);
        sniper = SniperCreator.buildSniper();
        TrainingStrategyImpl trainingStrategy = new TrainingStrategyImpl(new TrainingTerminalAction(options));
        observerImpl = new ObserverImpl(opponent, sniper, trainingStrategy);
        trainingInitializerImpl =mock(TrainingInitializerImpl.class);
    }

    @Test
    public void testCreateSniper() {
        when(trainingInitializerImpl.create()).thenReturn(Optional.of(observerImpl));
        TrainingDesignerFactoryImpl designerFactoryImpl = new TrainingDesignerFactoryImpl(trainingInitializerImpl, new TrainingAlertAction());
        Optional<TrainingDesigner> trainingDesigner = designerFactoryImpl.createSniper();
        assertThat(trainingDesigner.isPresent(), is(true));
    }

    @Test
    public void testLoadSniper() {
        when(trainingInitializerImpl.loadTraining()).thenReturn(Optional.of(observerImpl));
        TrainingDesignerFactoryImpl designerFactoryImpl = new TrainingDesignerFactoryImpl(trainingInitializerImpl, new TrainingAlertAction());
        Optional<TrainingDesigner> trainingDesigner = designerFactoryImpl.loadSniper();
        assertThat(trainingDesigner.isPresent(), is(true));
    }

    @Test
    public void testFetchSniper() {
        when(trainingInitializerImpl.fetchSniper()).thenReturn(Optional.of(sniper));
        TrainingDesignerFactoryImpl designerFactoryImpl = new TrainingDesignerFactoryImpl(trainingInitializerImpl, new TrainingAlertAction());
        Optional<Sniper> sniper = designerFactoryImpl.fetchSniper();
        assertThat(sniper.isPresent(), is(true));
    }

    @Test
    public void testDesignTrainingForSniper() {
        when(trainingInitializerImpl.fetchNewOpponent()).thenReturn(Optional.of(observerImpl));
        TrainingDesignerFactoryImpl designerFactoryImpl = new TrainingDesignerFactoryImpl(trainingInitializerImpl, new TrainingAlertAction());
        Optional<TrainingDesigner> trainingDesigner = designerFactoryImpl.designTrainingForSniper();
        assertThat(trainingDesigner.isPresent(), is(true));
    }
    
    @Test
    public void testDesignTrainingForSniperNull() {
        when(trainingInitializerImpl.fetchNewOpponent()).thenReturn(Optional.empty());
		TrainingDesignerFactoryImpl designerFactoryImpl = new TrainingDesignerFactoryImpl(trainingInitializerImpl,
				new TrainingAlertAction());
        Optional<TrainingDesigner> trainingDesigner = designerFactoryImpl.designTrainingForSniper();
        assertThat(trainingDesigner.isPresent(), is(false));
    }

}
