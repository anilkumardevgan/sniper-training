package com.training.sniper.game.training;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import com.training.sniper.game.builders.TerminalOutput;
import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.utils.SniperCreator;

public class TrainingTerminalActionTests extends TerminalOutput{

	private StartOptions<TrainingAction> startOptions;

	@Before
	@SuppressWarnings("unchecked")
	public void initialize() {
		startOptions = mock(StartOptions.class);
	}

	@Test
	public void testPrintSniper() {
		Sniper sniper = SniperCreator.buildSniper();
		TrainingTerminalAction trainingTerminalAction = new TrainingTerminalAction(startOptions);
		trainingTerminalAction.printSniper(sniper);
		String actual = terminalVal.toString();
		assertThat(actual, containsString(sniper.getSniperName()));
	}

	@Test
	public void testPrintOpponent() {
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		TrainingTerminalAction trainingTerminalAction = new TrainingTerminalAction(startOptions);
		trainingTerminalAction.printOpponent(opponent);
		String actual = terminalVal.toString();
		assertThat(actual, containsString(opponent.getSniperName()));
	}

	@Test
	public void testPrintMovement() {
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		TrainingTerminalAction trainingTerminalAction = new TrainingTerminalAction(startOptions);
		trainingTerminalAction.printMovement(opponent);
		String actual = terminalVal.toString();
		assertThat(actual, containsString(opponent.getSniperName()));
		assertThat(actual, containsString("moved from current position to another position"));
	}

	@Test
	public void testPrintDoNothing() {
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		TrainingTerminalAction trainingTerminalAction = new TrainingTerminalAction(startOptions);
		trainingTerminalAction.printDoNothing(opponent);
		String actual = terminalVal.toString();
		assertThat(actual, containsString(opponent.getSniperName()));
		assertThat(actual, containsString("watching opponent behavior"));

	}

	@Test
	public void testPrintShoot() {
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		Sniper sniper = SniperCreator.buildSniper();
		TrainingTerminalAction trainingTerminalAction = new TrainingTerminalAction(startOptions);
		trainingTerminalAction.printShoot(sniper, opponent, sniper.getMinDamageCapability());
		String actual = terminalVal.toString();
		assertThat(actual, containsString(opponent.getSniperName()));
		assertThat(actual, containsString("life damage"));
	}
}
