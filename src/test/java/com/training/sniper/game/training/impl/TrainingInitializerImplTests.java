package com.training.sniper.game.training.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.training.TrainingTerminalAction;
import com.training.sniper.game.training.factory.SniperFactory;
import com.training.sniper.game.training.observer.Observer;
import com.training.sniper.game.training.strategy.TrainingStrategy;
import com.training.sniper.game.training.strategy.TrainingStrategyImpl;
import com.training.sniper.game.utils.SniperCreator;

@RunWith(PowerMockRunner.class)
@PrepareForTest(TrainingInitializerImpl.class)
public class TrainingInitializerImplTests {

	private static final String NAME_OF_METHOD = "createOpponents";
	private SniperFactory sniperFactory = null;
	private TrainingStrategy trainingStrategy = null;
	private TrainingInitializerImpl trainingInitializer = null;

	private StartOptions<TrainingAction> startOptions = new StartOptions<>("Choose your action:",
			TrainingAction.values());

	@Before
	public void initialize() {
		sniperFactory = mock(SniperFactory.class);
		trainingStrategy = new TrainingStrategyImpl(new TrainingTerminalAction(startOptions));
		trainingInitializer = new TrainingInitializerImpl(sniperFactory, trainingStrategy);
	}

	@Test
	public void testCreate() throws Exception {
		when(sniperFactory.getSniper()).thenReturn(SniperCreator.buildSniper());
		TrainingInitializerImpl spyObj = PowerMockito.spy(trainingInitializer);
		PowerMockito.doReturn(SniperCreator.buildOpponent()).when(spyObj, NAME_OF_METHOD);
		Optional<Observer> optional = spyObj.create();
		PowerMockito.verifyPrivate(spyObj, Mockito.times(1)).invoke(NAME_OF_METHOD);
		assertNotNull(optional.get());
		assertNotNull(optional.get().getOpponent());
		assertNotNull(optional.get().getSniper());

	}

	@Test
	public void testfetchNewOpponentNull() {
		Optional<Observer> optional = trainingInitializer.fetchNewOpponent();
		assertThat(optional.isPresent(), is(false));
	}

	@Test
	public void testfetchNewOpponent() throws Exception {
		when(sniperFactory.getSniper()).thenReturn(SniperCreator.buildSniper());
		trainingInitializer.create();
		Optional<Observer> optional = trainingInitializer.fetchNewOpponent();
		assertThat(optional.isPresent(), is(true));
	}


	@Test
	public void testfetchSniper() throws Exception {
		when(sniperFactory.getSniper()).thenReturn(SniperCreator.buildSniper());
		TrainingInitializerImpl spyObj = PowerMockito.spy(trainingInitializer);
		PowerMockito.doReturn(SniperCreator.buildOpponent()).when(spyObj, NAME_OF_METHOD);
		spyObj.create();
		Optional<Sniper> sniper = spyObj.fetchSniper();
		assertThat(sniper.get(), is(notNullValue()));
	}

	@Test
	public void testfetchSniperEmpty() throws Exception {
		Optional<Sniper> sniper = trainingInitializer.fetchSniper();
		assertThat(sniper.isPresent(), is(false));
	}

}
