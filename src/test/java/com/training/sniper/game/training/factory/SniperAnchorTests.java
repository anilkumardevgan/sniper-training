package com.training.sniper.game.training.factory;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.terminal.unit.ValueInput;


public class SniperAnchorTests {
	
	@SuppressWarnings("unchecked")
	@Test
    public void testonCompletionSniperCreation() {
        ValueInput sniperName = mock(ValueInput.class);;
        StartOptions<GunType> gunType = mock(StartOptions.class);
        StartOptions<Mode> mode =  mock(StartOptions.class);
        StartOptions<DamageCapability> damageCapability =  mock(StartOptions.class);
        when(sniperName.getValue()).thenReturn("Anil Devgan");
        when(gunType.selectFromOptions()).thenReturn(GunType.HASKINS_RIFLE);
        when(mode.selectFromOptions()).thenReturn(Mode.CLASSIC);
        when(damageCapability.selectFromOptions()).thenReturn(DamageCapability.THIRTY);
        SniperAnchor sniperAnchor = new SniperAnchor(new SniperTerminal(sniperName,gunType,mode,damageCapability));
        Sniper sniper = sniperAnchor.getSniper();
        assertThat(sniper.getSniperName(), is("Anil Devgan"));
    }
}
