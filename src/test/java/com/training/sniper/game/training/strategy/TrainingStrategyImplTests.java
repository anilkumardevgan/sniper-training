package com.training.sniper.game.training.strategy;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.validateMockitoUsage;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.resources.ResourceManager;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.training.TrainingTerminalAction;
import com.training.sniper.game.utils.SniperCreator;


@RunWith(PowerMockRunner.class)
@PrepareForTest({TrainingStrategyImpl.class,ResourceManager.class})
public class TrainingStrategyImplTests {

    
    private StartOptions<TrainingAction> startOptions;
    private Sniper sniper;
    private  Sniper opponent;
    @SuppressWarnings("unchecked")
	@Before
    public void initialize() {
        startOptions = mock(StartOptions.class);
        this.sniper = SniperCreator.buildSniper();
        this.opponent = SniperCreator.buildOpponent().get(0);
    }

    @Test
    public void testStart() throws Exception {
        when(startOptions.selectFromOptions()).thenReturn(TrainingAction.SNIPER_ATTACK);
        TrainingTerminalAction trainingTerminalAction = mock(TrainingTerminalAction.class);
        PowerMockito.whenNew(TrainingTerminalAction.class).withArguments(startOptions).thenReturn(trainingTerminalAction);
        TrainingStrategyImpl spyObj = PowerMockito.spy(new TrainingStrategyImpl(trainingTerminalAction));
        spyObj.start(sniper, opponent);
        TrainingStatus trainingStatus = spyObj.getTrainingStatus();
        assertThat(trainingStatus, is(TrainingStatus.COMPLETED));
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("modifyStatus");
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("nextLoop");
    }

    @Test
    public void testOnSniperAttack() throws Exception {

        when(startOptions.selectFromOptions()).thenReturn(TrainingAction.SNIPER_ATTACK);
        TrainingTerminalAction trainingTerminalAction = mock(TrainingTerminalAction.class);
        PowerMockito.whenNew(TrainingTerminalAction.class).withArguments(startOptions).thenReturn(trainingTerminalAction);
        TrainingStrategyImpl spyObj = PowerMockito.spy(new TrainingStrategyImpl(trainingTerminalAction));
        spyObj.start(sniper, opponent);
        spyObj.onSniperAttack();
        TrainingStatus trainingStatus = spyObj.getTrainingStatus();
        assertThat(trainingStatus, is(TrainingStatus.COMPLETED));
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("modifyStatus");
        PowerMockito.verifyPrivate(spyObj, times(2)).invoke("nextLoop");
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("opponentRandomAction");
    }

    @Test
    public void testOnSniperMovement() throws Exception {
        when(startOptions.selectFromOptions()).thenReturn(TrainingAction.SNIPER_MOVEMENT);
        TrainingTerminalAction trainingTerminalAction = mock(TrainingTerminalAction.class);
        PowerMockito.whenNew(TrainingTerminalAction.class).withArguments(startOptions).thenReturn(trainingTerminalAction);
        TrainingStrategyImpl spyObj = PowerMockito.spy(new TrainingStrategyImpl(trainingTerminalAction));
        spyObj.start(sniper, opponent);
        spyObj.onSniperMovement();
        TrainingStatus trainingStatus = spyObj.getTrainingStatus();
        assertThat(trainingStatus, is(TrainingStatus.COMPLETED));
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("modifyStatus");
        PowerMockito.verifyPrivate(spyObj, times(2)).invoke("nextLoop");
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("opponentRandomAction");
    }

   @Test
    public void testOnSniperDoNothing() throws Exception {
        when(startOptions.selectFromOptions()).thenReturn(TrainingAction.DO_NOTHING);
        TrainingTerminalAction trainingTerminalAction = mock(TrainingTerminalAction.class);
        PowerMockito.whenNew(TrainingTerminalAction.class).withArguments(startOptions).thenReturn(trainingTerminalAction);
        TrainingStrategyImpl spyObj = PowerMockito.spy(new TrainingStrategyImpl(trainingTerminalAction));
        spyObj.start(sniper, opponent);
        spyObj.onSniperDoNothing();
        TrainingStatus trainingStatus = spyObj.getTrainingStatus();
        assertThat(trainingStatus, is(TrainingStatus.COMPLETED));
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("modifyStatus");
        PowerMockito.verifyPrivate(spyObj, times(2)).invoke("nextLoop");
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("opponentRandomAction");
    }

    @Test
    public void testonSaveAndExit() throws Exception {
        when(startOptions.selectFromOptions()).thenReturn(TrainingAction.SAVE_GAME);
        TrainingTerminalAction trainingTerminalAction = mock(TrainingTerminalAction.class);
        PowerMockito.whenNew(TrainingTerminalAction.class).withArguments(startOptions).thenReturn(trainingTerminalAction);
        PowerMockito.mockStatic(ResourceManager.class);
        PowerMockito.doNothing().when(ResourceManager.class, "saveResourse", any(),any());
        TrainingStrategyImpl spyObj = PowerMockito.spy(new TrainingStrategyImpl(trainingTerminalAction));
        spyObj.start(sniper, opponent);
        spyObj.onSaveAndExit();
        TrainingStatus trainingStatus = spyObj.getTrainingStatus();
        assertThat(trainingStatus, is(TrainingStatus.SAVED));
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("modifyStatus");
        PowerMockito.verifyPrivate(spyObj, times(1)).invoke("nextLoop");
    }

    
    @After
    public void completion() {
        validateMockitoUsage();
    }

}
