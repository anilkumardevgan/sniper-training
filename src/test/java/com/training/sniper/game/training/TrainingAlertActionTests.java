package com.training.sniper.game.training;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.training.sniper.game.builders.TerminalOutput;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.utils.SniperCreator;

public class TrainingAlertActionTests extends TerminalOutput {
	
	@Test(expected=UnsupportedOperationException.class)
	public void testPrint() {
		TrainingAlertAction alertAction = new TrainingAlertAction();
		alertAction.print();
	}
	@Test
	public void testDisplayWinnerAlert() {
		Sniper player = SniperCreator.buildSniper();
		TrainingAlertAction alertAction = new TrainingAlertAction();
		alertAction.displayWinnerAlert(player);
		assertThat(terminalVal.toString(), containsString(player.getSniperName()));
	}
	@Test
	public void testDisplayPausedAlert() {
		TrainingAlertAction alertAction = new TrainingAlertAction();
		alertAction.displayPausedAlert();
		assertThat(terminalVal.toString(), startsWith("Training has been paused"));
	}
}
