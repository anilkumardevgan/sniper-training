package com.training.sniper.game.training.observer;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.Mockito;

import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.training.strategy.TrainingStrategyImpl;
import com.training.sniper.game.utils.SniperCreator;

public class ObserverImplTests {

	@Test
	public void testBegin() {
		TrainingStrategyImpl trainingStrategy = mock(TrainingStrategyImpl.class);
		ObserverImpl observerImpl = new ObserverImpl(SniperCreator.buildOpponent().get(0), SniperCreator.buildSniper(),
				trainingStrategy);
		Mockito.doNothing().when(trainingStrategy).start(SniperCreator.buildSniper(),
				SniperCreator.buildOpponent().get(0));
		observerImpl.begin();
		assertThat(observerImpl.getSniper().getExperience().getTrainings().size(), is(1));
	}

	@Test
	public void testGetSniper() {
		Sniper sniper = SniperCreator.buildSniper();
		TrainingStrategyImpl trainingStrategy = mock(TrainingStrategyImpl.class);
		ObserverImpl observerImpl = new ObserverImpl(sniper,SniperCreator.buildOpponent().get(0),trainingStrategy);
		assertThat(sniper, is(observerImpl.getSniper()));
	}

	@Test
	public void testGetOpponent() {
		Sniper sniper = SniperCreator.buildSniper();
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		TrainingStrategyImpl trainingStrategy = mock(TrainingStrategyImpl.class);
		ObserverImpl observerImpl = new ObserverImpl(sniper,opponent,trainingStrategy);
		assertThat(opponent, is(observerImpl.getOpponent()));
	}

	@Test
	public void testGetTrainingStatus() {
		TrainingStatus trainingStatus = TrainingStatus.values()[0];
		Sniper sniper = SniperCreator.buildSniper();
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		TrainingStrategyImpl trainingStrategy = mock(TrainingStrategyImpl.class);
		when(trainingStrategy.getTrainingStatus()).thenReturn(trainingStatus);
		ObserverImpl observerImpl = new ObserverImpl(opponent, sniper, trainingStrategy);
		assertThat(observerImpl.getTrainingStatus(), is(trainingStatus));
	}

}
