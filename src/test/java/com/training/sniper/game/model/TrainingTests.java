package com.training.sniper.game.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;

import org.junit.Test;

import com.training.sniper.game.utils.SniperCreator;

public class TrainingTests {

    @Test
    public void testGetTrainingDate() {
        LocalDateTime dateNow = LocalDateTime.now();
        Training training = new Training(dateNow);
        assertThat(training.getTrainingTime(), is(dateNow));
    }

    @Test
    public void testGetWinner() {
        LocalDateTime dateNow = LocalDateTime.now();
        Training training = new Training(dateNow);
        Sniper sniper = SniperCreator.buildSniper();
        Sniper opponent = SniperCreator.buildOpponent().get(0);
        sniper.isHitBy(opponent);
        training.declareWinnerAndRunnerUp(sniper, opponent);
        assertThat(training.getWinner(), is(opponent.getSniperName()));
        assertThat(training.getRunnerUp(), is(sniper.getSniperName()));
    }

    @Test
    public void testGetRunnerUp() {
        LocalDateTime dateNow = LocalDateTime.now();
        Training training = new Training(dateNow);
        Sniper sniper = SniperCreator.buildSniper();
        Sniper opponent = SniperCreator.buildOpponent().get(0);
        sniper.isHitBy(opponent);
        training.declareWinnerAndRunnerUp(opponent, sniper);
        assertThat(training.getRunnerUp(), is(sniper.getSniperName()));
    }
}
