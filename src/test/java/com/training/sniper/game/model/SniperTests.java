package com.training.sniper.game.model;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.SniperLevel;
import com.training.sniper.game.utils.SniperCreator;

public class SniperTests {

	@Test
	public void testGetSniperLevel() {
		Sniper sniper = SniperCreator.buildSniper();
		assertThat(sniper.getSniperLevel(), is(SniperLevel.Level1));
		sniper.setSniperLevel(SniperLevel.Level8);
		assertThat(sniper.getSniperLevel(), is(SniperLevel.Level8));
	}

	@Test
	public void testGetGunTypes() {
		Sniper sniper = SniperCreator.buildSniper();
		assertThat(sniper.getGunTypes().get(0), is(GunType.BARRETT_M90));
	}

	@Test
	public void testCanPlay() {
		Sniper sniper = SniperCreator.buildSniper();
		assertThat(sniper.canPlay(), is(true));
	}

	@Test
	public void testIsHitBy() {
		Sniper sniper = SniperCreator.buildSniper();
		Sniper opponent = SniperCreator.buildOpponent().get(0);
		int lifePending = opponent.isHitBy(sniper);
		assertThat(lifePending, is(50));
		opponent.isHitBy(sniper);
		lifePending = opponent.isHitBy(sniper);
		assertThat(lifePending, is(50));
	}

}
