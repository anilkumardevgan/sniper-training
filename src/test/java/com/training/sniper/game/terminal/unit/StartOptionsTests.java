package com.training.sniper.game.terminal.unit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.terminal.unit.StartOptions;


@RunWith(PowerMockRunner.class)
@PrepareForTest(StartOptions.class)
public class StartOptionsTests {

	@Test
	public void testPrint() {
		OutputStream outputStream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(outputStream);
		System.setOut(printStream);
		StartOptions<TrainingStatus> startOptions = new StartOptions<>("Training Status", TrainingStatus.values());
		startOptions.print();
		assertThat(outputStream.toString(), containsString("Training Status"));
		assertThat(outputStream.toString(), containsString(TrainingStatus.COMPLETED.toString()));
		PrintStream originalOut = System.out;
		System.setOut(originalOut);
	}

	@Test
	public void testSelectFromOptions() throws Exception {
		StartOptions<TrainingStatus> startOptions = PowerMockito.spy(new StartOptions<>("Training Status", TrainingStatus.values()));
		PowerMockito.doReturn(0).when(startOptions, "readItemIndex");
		TrainingStatus fightStatus = startOptions.selectFromOptions();
		assertThat(fightStatus, is(TrainingStatus.IN_PROGRESS));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDrawShouldThrowExceptionWhenItemsListSizeLessThanOne() {
		new StartOptions<>("Empty", Empty.values());
	}

	public enum Empty {

	}

}
