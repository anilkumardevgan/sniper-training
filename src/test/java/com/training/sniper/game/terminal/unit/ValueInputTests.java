package com.training.sniper.game.terminal.unit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.training.sniper.game.builders.TerminalOutput;
import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.terminal.unit.ValueInput;


public class ValueInputTests extends TerminalOutput {
    
    @Test
    public void testDraw() {
        final String expected = Constants.SNIPER_NAME;
        ValueInput input = new ValueInput(expected);
        input.print();
        String actual = terminalVal.toString();
        assertThat(actual , containsString(expected));
    }

}
