package com.training.sniper.game.terminal.action;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import com.training.sniper.game.builders.TerminalOutput;
import com.training.sniper.game.enums.Enums.StartOptionsItem;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.action.StartOptionsAnchor;
import com.training.sniper.game.terminal.action.StartOptionsTerminal;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.training.impl.TrainingDesignerFactoryImpl;
import com.training.sniper.game.training.impl.TrainingDesignerImpl;
import com.training.sniper.game.utils.SniperCreator;


public class StartOptionsAnchorTests extends TerminalOutput{

    private StartOptions<StartOptionsItem> startOptions;
    private StartOptionsTerminal startOptionsCV;
    private TrainingDesignerImpl trainingDesignerImpl;
    private TrainingDesignerFactoryImpl trainingDesignerFactoryImpl;
    private StartOptionsAnchor optionsAnchor;

    @SuppressWarnings("unchecked")
	@Before
    public void setUp() {
        this.startOptions = mock(StartOptions.class);
        this.startOptionsCV = new StartOptionsTerminal(startOptions);
        this.trainingDesignerFactoryImpl = mock(TrainingDesignerFactoryImpl.class);
        this.trainingDesignerImpl = mock(TrainingDesignerImpl.class);
        this.optionsAnchor = new StartOptionsAnchor(startOptionsCV, trainingDesignerFactoryImpl);
    }

    @Test
    public void testOnNewSniperTraining() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.NEW_TRAINING);
        when(trainingDesignerFactoryImpl.createSniper()).thenReturn(Optional.of(trainingDesignerImpl));
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onNewSniperTraining();
    }

    @Test
    public void testOnNewSniperTrainingNull() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.NEW_TRAINING);
        when(trainingDesignerFactoryImpl.createSniper()).thenReturn(Optional.empty());
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onNewSniperTraining();
        String actual = terminalVal.toString();
        assertThat(actual , containsString("Sorry Unable to create game now"));
    }

    @Test
    public void testOnResumeSavedTraining() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.RESUME_SAVED_GAME);
        when(trainingDesignerFactoryImpl.loadSniper()).thenReturn(Optional.of(trainingDesignerImpl));
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onResumeSavedTraining();
        String actual = terminalVal.toString();
        assertEquals(actual,"");
    }

    @Test
    public void testOnResumeSavedTrainingNull() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.RESUME_SAVED_GAME);
        when(trainingDesignerFactoryImpl.loadSniper()).thenReturn(Optional.empty());
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onResumeSavedTraining();
        String actual = terminalVal.toString();
        assertThat(actual , containsString("No previous game saved."));
    }

    @Test
    public void testOnViewSniperProfile() {
        Sniper sniper = SniperCreator.buildSniper();
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.VIEW_SNIPER_PROFILE);
        when(trainingDesignerFactoryImpl.fetchSniper()).thenReturn(Optional.of(sniper));
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onViewSniperProfile();
    }

    @Test
    public void testOnViewSniperProfileNull() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.VIEW_SNIPER_PROFILE);
        when(trainingDesignerFactoryImpl.fetchSniper()).thenReturn(Optional.empty());
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onViewSniperProfile();
        String actual = terminalVal.toString();
        assertThat(actual , containsString("Profile does not exist. Please, try making a new Sniper."));
    }

    @Test
    public void testOnNextSniperTrainingExistingProfile() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.NEXT_TRAINING);
        when(trainingDesignerFactoryImpl.designTrainingForSniper()).thenReturn(Optional.of(trainingDesignerImpl));
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onNextSniperTrainingExistingProfile();
    }

    @Test
    public void testonNextSniperTrainingExistingProfileNull() {
        when(this.startOptions.selectFromOptions()).thenReturn(StartOptionsItem.NEXT_TRAINING);
        when(trainingDesignerFactoryImpl.designTrainingForSniper()).thenReturn(Optional.empty());
        doNothing().when(trainingDesignerImpl).begin();
        optionsAnchor = spy(optionsAnchor);
        doNothing().when(optionsAnchor).display();
        optionsAnchor.onNextSniperTrainingExistingProfile();
        String actual = terminalVal.toString();
        assertThat(actual , containsString("Profile does not exist. Please, try making a new Sniper"));
    }


}
