package com.training.sniper.game.builders;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;

public class TerminalOutput {

	protected final ByteArrayOutputStream terminalVal = new ByteArrayOutputStream();

	@Before
	public void startOut() {
		System.setOut(new PrintStream(terminalVal));
	}

	@After
	public void restOut() {
		System.setOut(System.out);
	}
}
