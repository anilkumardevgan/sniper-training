package com.training.sniper.game.training.factory;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.terminal.unit.AbstractTerminal;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.terminal.unit.ValueInput;
import com.training.sniper.game.utils.OutputPrinter;

public class SniperTerminal extends AbstractTerminal<SniperAction.Actionable> implements SniperAction {

	private final StartOptions<GunType> gunType;

	private final StartOptions<Mode> mode;

	private final StartOptions<DamageCapability> damageCapability;

	private final ValueInput sniperName;

	public SniperTerminal(final ValueInput sniperName, final StartOptions<GunType> gunType, final StartOptions<Mode> mode,
			final StartOptions<DamageCapability> damageCapability) {
		this.sniperName = sniperName;
		this.damageCapability = damageCapability;
		this.gunType = gunType;
		this.mode = mode;
	}

	@Override
	public void print() {
		OutputPrinter.printOutput(Constants.CREATE_SNIPER_OPTIONS);

		sniperName.print();
		member.onSelection(sniperName.getValue());

		gunType.print();
		member.onSelection(gunType.selectFromOptions());

		mode.print();
		member.onSelection(mode.selectFromOptions());

		damageCapability.print();
		member.onSelection(damageCapability.selectFromOptions());

		member.onCompletion();

	}

}
