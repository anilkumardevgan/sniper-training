package com.training.sniper.game.training;

import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.TerminalAction;

public interface TrainingPrintAction extends TerminalAction<TrainingPrintAction.Actionable> {
	
	void printSniper(Sniper sniper);

    void printOpponent(Sniper opponent);

    void printMovement(Sniper player);

    void printDoNothing(Sniper player);

    void printShoot(Sniper attacker, Sniper defender, int damage);

    interface Actionable {
        void onSniperAttack();

        void onSniperMovement();

        void onSniperDoNothing();

        void onSaveAndExit();
    }

}
