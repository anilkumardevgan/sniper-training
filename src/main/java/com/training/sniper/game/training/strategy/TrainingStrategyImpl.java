package com.training.sniper.game.training.strategy;

import java.io.IOException;
import java.util.EnumSet;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.resources.ResourceManager;
import com.training.sniper.game.terminal.unit.AbstractAnchor;
import com.training.sniper.game.training.TrainingPrintAction;
import com.training.sniper.game.utils.RandomEnumSelector;

public class TrainingStrategyImpl extends AbstractAnchor<TrainingPrintAction>
		implements TrainingStrategy, TrainingPrintAction.Actionable {

	public TrainingStrategyImpl(final TrainingPrintAction action) {
		super(action);
		this.action.setMember(this);
	}

	private Sniper sniper;

	private Sniper opponent;

	private TrainingStatus trainingStatus;

	@Override
	public void start(Sniper sniper, Sniper opponent) {
		this.sniper = sniper;
		this.opponent = opponent;
		modifyStatus();
	}

	@Override
	public TrainingStatus getTrainingStatus() {
		return trainingStatus;
	}

	@Override
	public void onSniperAttack() {
		action.printShoot(sniper, opponent, opponent.isHitBy(sniper));
		if (!opponent.canPlay()) {
			return;
		}
		opponentRandomAction();
		nextLoop();
	}

	@Override
	public void onSniperMovement() {
		sniper.movement();
		action.printMovement(sniper);
		opponentRandomAction();
		nextLoop();

	}

	@Override
	public void onSniperDoNothing() {
		action.printDoNothing(sniper);
		opponentRandomAction();
		nextLoop();
	}

	@Override
	public void onSaveAndExit() {
		try {
			ResourceManager.saveResourse(Constants.USER_SNIPER, sniper);
			ResourceManager.saveResourse(Constants.OPPONENT_SNIPER, opponent);
			this.trainingStatus = TrainingStatus.SAVED;
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void modifyStatus() {
		this.trainingStatus = TrainingStatus.IN_PROGRESS;
		nextLoop();
		if (this.trainingStatus != TrainingStatus.SAVED) {
			this.trainingStatus = TrainingStatus.COMPLETED;
		}
	}

	private void nextLoop() {
		if (sniper.canPlay() && opponent.canPlay()) {
			action.printSniper(sniper);
			action.printOpponent(opponent);
			display();
		}
	}

	private void opponentRandomAction() {
		final EnumSet<TrainingAction> trainingActionEnum = EnumSet.allOf(TrainingAction.class);
		trainingActionEnum.remove(TrainingAction.SAVE_GAME);
		final TrainingAction trainingAction = RandomEnumSelector.randomEnum(trainingActionEnum);
		switch (trainingAction) {
		case SNIPER_ATTACK:
			action.printShoot(opponent, sniper, sniper.isHitBy(opponent));
			break;
		case SNIPER_MOVEMENT:
			opponent.movement();
			action.printMovement(opponent);
			break;
		case DO_NOTHING:
			action.printDoNothing(opponent);
			break;
		default:
			throw new IllegalArgumentException(Constants.NO_ACTION_SPECIFIED + action);
		}
	}

}
