package com.training.sniper.game.training.factory;

import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.terminal.unit.TerminalAction;

public interface SniperAction extends TerminalAction<SniperAction.Actionable>{
	interface Actionable{
		
		void onSelection(Mode mode);

        void onSelection(GunType guntype);

        void onSelection(String name);

        void onSelection(DamageCapability damageCapability);

        void onCompletion();
	}
}
