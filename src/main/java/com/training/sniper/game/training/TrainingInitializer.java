package com.training.sniper.game.training;

import java.util.Optional;

import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.training.observer.Observer;

public interface TrainingInitializer {
	Optional<Observer> create();

	Optional<Observer> loadTraining();

	Optional<Observer> fetchNewOpponent();

	Optional<Sniper> fetchSniper();
}
