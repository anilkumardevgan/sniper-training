package com.training.sniper.game.training.observer;

import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;

public interface Observer {
	
	void begin();

	Sniper getSniper();

	Sniper getOpponent();

	TrainingStatus getTrainingStatus();

	void declareWinnerAndRunnerUp();

}
