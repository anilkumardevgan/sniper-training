package com.training.sniper.game.training;

import java.util.Optional;

import com.training.sniper.game.model.Sniper;


public interface TrainingDesignerFactory {
	Optional<TrainingDesigner> createSniper();

    Optional<TrainingDesigner> loadSniper();

    Optional<TrainingDesigner> designTrainingForSniper();

    Optional<Sniper> fetchSniper();
}
