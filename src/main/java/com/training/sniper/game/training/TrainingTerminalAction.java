package com.training.sniper.game.training;

import static java.lang.String.format;

import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.AbstractTerminal;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.training.TrainingPrintAction.Actionable;
import com.training.sniper.game.utils.OutputPrinter;

public class TrainingTerminalAction extends AbstractTerminal<Actionable> implements TrainingPrintAction {

	private final StartOptions<TrainingAction> startOptions;

	public TrainingTerminalAction(final StartOptions<TrainingAction> startOptions) {
		super();
		this.startOptions = startOptions;
	}

	@Override
	public void print() {
		startOptions.print();
		switch (startOptions.selectFromOptions()) {
		case SNIPER_ATTACK:
			member.onSniperAttack();
			break;
		case SNIPER_MOVEMENT:
			member.onSniperMovement();
			break;
		case DO_NOTHING:
			member.onSniperDoNothing();
			break;
		case SAVE_GAME:
			member.onSaveAndExit();
			break;
		default:
		}
	}

	@Override
	public void printSniper(Sniper sniper) {
		printPlayer(sniper);
	}

	@Override
	public void printOpponent(Sniper opponent) {
		printPlayer(opponent);
	}

	private void printPlayer(final Sniper sniper) {
		OutputPrinter.printOutput(
				format("Name: %s;  Mode: %s;  Damage Capability: %d;  Sniper Level: %s;" + "  Life pending: %d;",
						sniper.getSniperName(), sniper.getMode(), sniper.getMinDamageCapability(),
						sniper.getSniperLevel(), sniper.getLife()));
	}

	@Override
	public void printMovement(Sniper player) {
		OutputPrinter.printOutput(format("%s;  moved from current position to another position", player.getSniperName()));

	}

	@Override
	public void printDoNothing(Sniper player) {
		OutputPrinter.printOutput(format("%s; is watching opponent behavior.", player.getSniperName()));

	}

	@Override
	public void printShoot(Sniper attacker, Sniper defender, int damage) {
		OutputPrinter.printOutput(format("%s shoots %s. %s got a life damage of - %d", attacker.getSniperName(),
				defender.getSniperName(), defender.getSniperName(), damage));

	}

}
