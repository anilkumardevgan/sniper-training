package com.training.sniper.game.training;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.AbstractTerminal;
import com.training.sniper.game.terminal.unit.AlertAction;
import com.training.sniper.game.utils.OutputPrinter;

public class TrainingAlertAction extends AbstractTerminal<AlertAction.Actionable>
implements AlertAction {

	@Override
	public void print() {
		 throw new UnsupportedOperationException(Constants.METHOD_NOT_SUPPORTED);
	}
	@Override
	public void displayWinnerAlert(Sniper sniper) {
		OutputPrinter.printOutput(Constants.TRAINING_WINNER+sniper.getSniperName());
	}

	@Override
	public void displayPausedAlert() {
		OutputPrinter.printOutput(Constants.TRAINING_PAUSED);
		
	}

}
