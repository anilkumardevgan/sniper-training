package com.training.sniper.game.training.impl;

import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.AlertAction;
import com.training.sniper.game.training.TrainingDesigner;
import com.training.sniper.game.training.observer.Observer;

public class TrainingDesignerImpl implements TrainingDesigner {

	private final Observer observer;

	private final AlertAction alertAction;

	public TrainingDesignerImpl(final Observer observer, final AlertAction alertAction) {
		this.observer = observer;
		this.alertAction = alertAction;
	}

	@Override
	public void begin() {
		observer.begin();
		showAlert();
	}

	private void showAlert() {
		final TrainingStatus trainingStatus = this.observer.getTrainingStatus();
		switch (trainingStatus) {
		case COMPLETED:
			displayWinnerAlert();
			break;
		case SAVED:
			alertAction.displayPausedAlert();
			break;
		default:
			break;
		}
	}

	private void displayWinnerAlert() {
		final Sniper opponent = this.observer.getOpponent();
		final Sniper sniper = this.observer.getSniper();
		Sniper winner;
		observer.declareWinnerAndRunnerUp();
		if (sniper != null && opponent != null) {
			if (sniper.getLife() > opponent.getLife()) {
				winner = sniper;
			} else {
				winner = opponent;
			}
			alertAction.displayWinnerAlert(winner);
		}
	}

}
