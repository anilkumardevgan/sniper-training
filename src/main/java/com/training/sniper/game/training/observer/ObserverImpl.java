package com.training.sniper.game.training.observer;

import java.time.LocalDateTime;

import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.model.Training;
import com.training.sniper.game.training.strategy.TrainingStrategy;

public class ObserverImpl implements Observer {

	private final TrainingStrategy trainingStrategy;

	private final Sniper sniper;

	private final Sniper opponent;

	private Training training;

	public ObserverImpl(final Sniper sniper, final Sniper opponent, final TrainingStrategy trainingStrategy) {
		this.trainingStrategy = trainingStrategy;
		this.sniper = sniper;
		this.opponent = opponent;
	}

	@Override
	public void begin() {
		this.training = new Training(LocalDateTime.now());
		trainingStrategy.start(this.sniper, this.opponent);
		this.sniper.getExperience().getTrainings().add(training);
		this.opponent.getExperience().getTrainings().add(training);
	}

	@Override
	public Sniper getSniper() {
		return this.sniper;
	}

	@Override
	public Sniper getOpponent() {
		return this.opponent;
	}

	@Override
	public TrainingStatus getTrainingStatus() {
		return this.trainingStrategy.getTrainingStatus();
	}

	@Override
	public void declareWinnerAndRunnerUp() {
		this.training.declareWinnerAndRunnerUp(sniper, opponent);

	}
}
