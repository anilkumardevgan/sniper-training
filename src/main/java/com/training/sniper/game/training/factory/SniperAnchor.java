package com.training.sniper.game.training.factory;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.AbstractAnchor;
import com.training.sniper.game.training.factory.SniperAction.Actionable;

public class SniperAnchor extends AbstractAnchor<SniperAction> implements SniperFactory, Actionable {

	private GunType guntype;

	private Mode mode;

	private String name;

	private DamageCapability damageCapability;

	public SniperAnchor(final SniperAction action) {
		super(action);
		action.setMember(this);
	}

	@Override
	public void onSelection(Mode mode) {
		this.mode = mode;
	}

	@Override
	public void onSelection(GunType guntype) {
		this.guntype = guntype;
	}

	@Override
	public void onSelection(String name) {
		this.name = name;

	}

	@Override
	public void onSelection(DamageCapability damageCapability) {
		this.damageCapability = damageCapability;

	}

	@Override
	public void onCompletion() {
		requireNonNull(name, Constants.NAME_REQUIRED);
		requireNonNull(guntype, Constants.GUN_TYPE_REQUIRED);
		requireNonNull(mode, Constants.MODE_REQUIRED);
		requireNonNull(damageCapability, Constants.DC_REQUIRED);

	}

	@Override
	public Sniper getSniper() {
		display();
		final List<GunType> gunTypes = new ArrayList<>();
		gunTypes.add(this.guntype);
		Sniper sniper = new Sniper(name, mode, damageCapability.getValue(), gunTypes);
		return sniper;
	}

}
