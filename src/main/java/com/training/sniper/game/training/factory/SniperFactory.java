package com.training.sniper.game.training.factory;

import com.training.sniper.game.model.Sniper;

public interface SniperFactory {
	Sniper getSniper();
}
