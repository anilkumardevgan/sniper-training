package com.training.sniper.game.training.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.enums.Enums.Opponent;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.resources.ResourceManager;
import com.training.sniper.game.training.TrainingInitializer;
import com.training.sniper.game.training.factory.SniperFactory;
import com.training.sniper.game.training.observer.Observer;
import com.training.sniper.game.training.observer.ObserverImpl;
import com.training.sniper.game.training.strategy.TrainingStrategy;
import com.training.sniper.game.utils.RandomEnumSelector;
import com.training.sniper.game.utils.RandomIntSelector;

public class TrainingInitializerImpl implements TrainingInitializer {

	private Sniper sniper;

	private Sniper opponent;

	private List<Sniper> opponents;

	private final SniperFactory sniperFactory;

	private final TrainingStrategy trainingStrategy;

	public TrainingInitializerImpl(final SniperFactory sniperFactory, final TrainingStrategy trainingStrategy) {
		this.sniperFactory = sniperFactory;
		this.trainingStrategy = trainingStrategy;
	}

	@Override
	public Optional<Observer> create() {
		this.sniper = this.sniperFactory.getSniper();
		this.opponents = createOpponents();
		this.opponent = this.opponents.get(RandomIntSelector.getRandomIntFromRange(0, opponents.size()));
		return Optional.of(new ObserverImpl(this.sniper, this.opponent, trainingStrategy));
	}

	@Override
	public Optional<Observer> loadTraining() {
		try {
			this.sniper = (Sniper) ResourceManager.fetchResourse(Constants.USER_SNIPER);
			this.opponent = (Sniper) ResourceManager.fetchResourse(Constants.OPPONENT_SNIPER);
			if (this.opponents == null) {
				this.opponents = createOpponents();
			}
			return Optional.of(new ObserverImpl(this.sniper, opponent, trainingStrategy));
		} catch (ClassNotFoundException | IOException e) {
			System.err.println(e.getMessage());
		}
		return Optional.empty();
	}

	@Override
	public Optional<Observer> fetchNewOpponent() {
		if (this.opponents != null) {
			this.opponent = this.opponents.get(RandomIntSelector.getRandomIntFromRange(0, this.opponents.size()));
			return Optional.of(new ObserverImpl(this.sniper, this.opponent, trainingStrategy));
		}
		return Optional.empty();
	}

	@Override
	public Optional<Sniper> fetchSniper() {
		if (this.sniper != null) {
			return Optional.of(this.sniper);
		}
		return Optional.empty();
	}

	private List<Sniper> createOpponents() {
		List<Sniper> opponents = new ArrayList<>();
		Sniper opponent = null;
		for (final Opponent opp : Opponent.values()) {
			final DamageCapability damageCapability = RandomEnumSelector
					.randomEnum(EnumSet.allOf(DamageCapability.class));
			opponent = new Sniper(opp.toString(), getRandomMode(), damageCapability.getValue(), getGunTypes());
			opponents.add(opponent);
		}

		return opponents;

	}

	private Mode getRandomMode() {
		final int maxIndex = Mode.values().length;
		return Mode.values()[RandomIntSelector.getRandomIntFromRange(0, maxIndex)];
	}

	private List<GunType> getGunTypes() {
		final int maxIndex = GunType.values().length;
		final List<GunType> list = new ArrayList<>();
		list.add(GunType.values()[RandomIntSelector.getRandomIntFromRange(0, maxIndex)]);
		return list;
	}

}
