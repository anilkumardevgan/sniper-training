package com.training.sniper.game.training.impl;

import java.util.Optional;

import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.AlertAction;
import com.training.sniper.game.training.TrainingDesigner;
import com.training.sniper.game.training.TrainingDesignerFactory;
import com.training.sniper.game.training.TrainingInitializer;
import com.training.sniper.game.training.observer.Observer;

public class TrainingDesignerFactoryImpl implements TrainingDesignerFactory {

	final private TrainingInitializer trainingInitializer;
	final private AlertAction alertAction;

	public TrainingDesignerFactoryImpl(final TrainingInitializer trainingInitializer, final AlertAction alertAction) {
		this.trainingInitializer = trainingInitializer;
		this.alertAction = alertAction;
	}

	@Override
	public Optional<TrainingDesigner> createSniper() {
		return create(trainingInitializer.create());
	}

	private Optional<TrainingDesigner> create(final Optional<Observer> optional) {
		if (optional.isPresent()) {
			return Optional.of(new TrainingDesignerImpl(optional.get(), alertAction));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Optional<TrainingDesigner> loadSniper() {
		return create(trainingInitializer.loadTraining());
	}

	@Override
	public Optional<TrainingDesigner> designTrainingForSniper() {
		return create(trainingInitializer.fetchNewOpponent());
	}

	@Override
	public Optional<Sniper> fetchSniper() {
		return this.trainingInitializer.fetchSniper();
	}

}
