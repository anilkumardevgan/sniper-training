package com.training.sniper.game.training.strategy;

import com.training.sniper.game.enums.Enums.TrainingStatus;
import com.training.sniper.game.model.Sniper;

public interface TrainingStrategy {

	void start(Sniper sniper, Sniper opponent);

	TrainingStatus getTrainingStatus();
}
