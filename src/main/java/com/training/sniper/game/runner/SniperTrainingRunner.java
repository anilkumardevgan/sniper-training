package com.training.sniper.game.runner;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.enums.Enums.StartOptionsItem;
import com.training.sniper.game.enums.Enums.TrainingAction;
import com.training.sniper.game.terminal.action.StartOptionsAnchor;
import com.training.sniper.game.terminal.action.StartOptionsTerminal;
import com.training.sniper.game.terminal.unit.AlertAction;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.terminal.unit.ValueInput;
import com.training.sniper.game.training.TrainingTerminalAction;
import com.training.sniper.game.training.TrainingAlertAction;
import com.training.sniper.game.training.TrainingDesignerFactory;
import com.training.sniper.game.training.TrainingInitializer;
import com.training.sniper.game.training.factory.SniperAnchor;
import com.training.sniper.game.training.factory.SniperTerminal;
import com.training.sniper.game.training.impl.TrainingDesignerFactoryImpl;
import com.training.sniper.game.training.impl.TrainingInitializerImpl;
import com.training.sniper.game.training.strategy.TrainingStrategy;
import com.training.sniper.game.training.strategy.TrainingStrategyImpl;

public class SniperTrainingRunner {

	public static void start() {
	final StartOptions<StartOptionsItem> startOptions = new StartOptions<>(Constants.WELCOME,StartOptionsItem.values());
	new StartOptionsAnchor(new StartOptionsTerminal(startOptions), trainingDesignerFactory()).display();
	}
	
	private static TrainingDesignerFactory trainingDesignerFactory() {
        return new TrainingDesignerFactoryImpl(trainingInitializer(),trainingAction());
    }
	
	private static TrainingInitializer trainingInitializer() {
        return new TrainingInitializerImpl(sniperAnchor(),trainingStrategy());
	}
	
	private static AlertAction trainingAction() {
		return new TrainingAlertAction();
	}
	
	private static SniperAnchor sniperAnchor() {
		final ValueInput sniperName = new ValueInput(Constants.SNIPER_NAME);
		final StartOptions<GunType> gunType = new StartOptions<>(Constants.SNIPER_GUN, GunType.values());
		final StartOptions<Mode> mode = new StartOptions<>(Constants.SNIPER_MODE, Mode.values());
		final StartOptions<DamageCapability> damageCapability = new StartOptions<>(Constants.SNIPER_DC,DamageCapability.values());
		return new SniperAnchor(new SniperTerminal(sniperName, gunType, mode, damageCapability));
	}
	private static TrainingStrategy trainingStrategy() {
		StartOptions<TrainingAction> startOptions = new StartOptions<>(Constants.SNIPER_ACTION, TrainingAction.values());
		return new TrainingStrategyImpl(new TrainingTerminalAction(startOptions));
	}
	
}
