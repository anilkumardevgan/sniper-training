package com.training.sniper.game.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Experience implements Serializable {

	private static final long serialVersionUID = 7627454369706459932L;
	private List<Training> trainings;

	public Experience() {
		this.trainings = new ArrayList<>();
	}
	public List<Training> getTrainings() {
		return trainings;
	}
	@Override
	public String toString() {
		return "Experience= [trainings = " + trainings + "]";
	}
}
