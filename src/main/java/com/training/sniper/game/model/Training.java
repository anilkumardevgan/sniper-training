package com.training.sniper.game.model;

import java.io.Serializable;
import java.time.LocalDateTime;


public class Training implements Serializable{
	
	private static final long serialVersionUID = -4302582690687840472L;

	private String winner;

	private String runnerUp;
	
	private LocalDateTime trainingTime;

	public Training(final LocalDateTime trainingTime) {
        this.trainingTime = trainingTime;
    }

	public String getWinner() {
		return winner;
	}

	public String getRunnerUp() {
		return runnerUp;
	}

	public LocalDateTime getTrainingTime() {
		return trainingTime;
	}
	
	public void declareWinnerAndRunnerUp(final Sniper player1,final Sniper player2) {
        if(player1.getLife()>player2.getLife()) {
            winner = player1.getSniperName();
            runnerUp = player2.getSniperName();
        }else if(player1.getLife()<player2.getLife()){
            winner = player2.getSniperName();
            runnerUp = player1.getSniperName();
        }
    }

	@Override
	public String toString() {
		return "Training [winner=" + winner + ", runnerUp=" + runnerUp + ", trainingTime=" + trainingTime + "]";
	}


}
