package com.training.sniper.game.model;

import java.io.Serializable;
import java.util.EnumSet;
import java.util.List;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.enums.Enums.Movement;
import com.training.sniper.game.enums.Enums.SniperLevel;
import com.training.sniper.game.utils.RandomEnumSelector;

public class Sniper implements Serializable {

	private static final long serialVersionUID = -3318319756012789747L;

	private final String sniperName;

	private final Mode mode;

	private final Experience experience;

	private int life;

	private SniperLevel sniperLevel;

	private final List<GunType> gunTypes;

	private final int minDamageCapability;

	private Movement movement;

	public Sniper(final String sniperName, final Mode mode, final int minDamageCapability,
			final List<GunType> gunTypes) {
		if (sniperName == null || "".equals(sniperName.trim())) {
			this.sniperName = Constants.TEST_SNIPER;
		} else {
			this.sniperName = sniperName;
		}
		this.mode = mode;
		this.setSniperLevel(SniperLevel.Level1);
		this.minDamageCapability = minDamageCapability;
		this.experience = new Experience();
		this.gunTypes = gunTypes;
		this.life = 100;
	}

	public boolean canPlay() {
		return life > 0;
	}

	public void movement() {
		this.movement = RandomEnumSelector.randomEnum(EnumSet.allOf(Movement.class));
	}

	public int isHitBy(final Sniper sniper) {
		int hittingForce = sniper.getMinDamageCapability();
		if (this.movement != null) {
			hittingForce = (int) (hittingForce * this.movement.getValue());
			this.movement = null;
		}
		if (hittingForce < this.life) {
			this.life -= hittingForce;
			return hittingForce;
		}
		this.life = 0;
		return hittingForce;
	}

	public String getSniperName() {
		return sniperName;
	}

	public Mode getMode() {
		return mode;
	}

	public Experience getExperience() {
		return experience;
	}

	public int getLife() {
		return life;
	}

	public SniperLevel getSniperLevel() {
		return sniperLevel;
	}

	public List<GunType> getGunTypes() {
		return gunTypes;
	}

	public void setSniperLevel(SniperLevel sniperLevel) {
		this.sniperLevel = sniperLevel;
	}

	public int getMinDamageCapability() {
		return minDamageCapability;
	}

	@Override
	public String toString() {
		return "Sniper [sniperName=" + sniperName + ", mode=" + mode + ", experience=" + experience + ", life=" + life
				+ ", sniperLevel=" + sniperLevel + ", gunTypes=" + gunTypes + ", minDamageCapability="
				+ minDamageCapability + "]";
	}
}
