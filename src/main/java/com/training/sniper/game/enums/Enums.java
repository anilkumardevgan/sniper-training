package com.training.sniper.game.enums;

public class Enums {
	
	public enum StartOptionsItem {
		NEW_TRAINING("Start sniper training with new Sniper"), NEXT_TRAINING(
				"Start a new training with existing Sniper"), RESUME_SAVED_GAME(
						"Load saved Sniper training module"), VIEW_SNIPER_PROFILE(
								"View Sniper's current profile."), EXIT("Exit the game");
		private final String val;

		StartOptionsItem(String val) {
			this.val = val;
		}

		@Override
		public String toString() {
			return val;
		}
	}

	public enum TrainingStatus {
		IN_PROGRESS("In progress"), SAVED("Paused and saved."), COMPLETED("Completed");

		private final String value;

		TrainingStatus(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum TrainingAction {
		SNIPER_ATTACK("Strike your Opponent with your gun."), SNIPER_MOVEMENT(
				"Move yourself to different location."), DO_NOTHING(
						"Stay at current position and observe."), SAVE_GAME("Save and exit training.");
		;

		private final String value;

		TrainingAction(final String title) {
			this.value = title;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum Movement {
		HIDING(.65), SLIDING(.70), WALKING(.75), MOVING(.80), JOGGING(.90), RUNNING(.95);
		private final Double value;

		Movement(final Double value) {
			this.value = value;
		}

		public double getValue() {
			return this.value;
		}

		@Override
		public String toString() {
			return Double.toString(value);
		}
	}

	public enum Opponent {
		ADELBERT_WALDRON("Adelbert Waldron"), VASILY_ZAYTSEV("Vasily Zaytsev"), CHRIS_KYLE("Chris Kyle"), CARLOS_NORMAN(
				"Carlos Norman"), SIMO_HAYHA("Simo Hayha"), HIRAM_BERDAN(
						"Hiram Berdan"), NOAH_ADAMIA("Noah Adamia"), ROB_FURLONG("Rob Furlong");
		private final String value;

		Opponent(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum Mode {
		CLASSIC("Classic"), ARCADE("Arcade");
		private final String value;

		Mode(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum DamageCapability {
		TEN(10), TWENTY(20), THIRTY(30), FOURTY(40), FIFTY(50);
		private final Integer value;

		DamageCapability(final Integer value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}

		@Override
		public String toString() {
			return Integer.toString(value);
		}
	}

	public enum GunType {
		ARCTIC_WARFARE_AS50("Arctic Warfare AS50"), ALEJANDRO_SNIPER_RIFLE("Alejandro Sniper Rifle"), BARRETT_M90(
				"Barrett M90"), SNIPER_MAGNUM("Sniper Magnum"), HASKINS_RIFLE("Haskins Rifle");
		private final String value;

		GunType(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	public enum SniperLevel {
		Level1("Newbie"), Level2("Beginner"), Level3("Talented"), Level4("Skilled"), Level5("Proficient"), Level6(
				"Experienced"), Level7("Advanced"), Level8("Senior"), Level9("Expert"), Level10("Pro Player"),;
		private final String value;

		SniperLevel(final String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

}
