package com.training.sniper.game.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum PropertyFileHandler {
	INSTANCE;
    private Properties file = new Properties();
    private PropertyFileHandler() {
        InputStream inputStream;
        final String propfName = "application.properties";
        inputStream = getClass().getClassLoader().getResourceAsStream(propfName);
        if (inputStream != null) {
            try {
            	file.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } 
    }
    public String getPropertyValue(final String value) {
        return file.getProperty(value);
    }
}
