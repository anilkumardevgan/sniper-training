package com.training.sniper.game.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.training.sniper.game.config.PropertyFileHandler;
import com.training.sniper.game.constants.Constants;


public class ResourceManager {

    private static final String PATH =  PropertyFileHandler.INSTANCE.getPropertyValue(Constants.RES_FILE_PATH); ;
    private ResourceManager() {
    }
    public static void saveResourse(final String fName,final Object object) throws IOException {
    	 final File folder = new File(PATH);
    	 if(!folder.exists()){
    		 folder.mkdir();
    	 }
    	final File file = new File(PATH+fName);
        if(!file.exists()) {
            file.createNewFile();
        }
        try(FileOutputStream fileOS = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOS);){
            out.writeObject(object);
        }
    }

    public static Object fetchResourse(final String fName) throws IOException, ClassNotFoundException {
        try(FileInputStream file = new FileInputStream(PATH+fName);
        		ObjectInputStream in = new ObjectInputStream(file);){
           return in.readObject();
        }
    }


}
