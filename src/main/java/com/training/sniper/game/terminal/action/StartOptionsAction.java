package com.training.sniper.game.terminal.action;

import com.training.sniper.game.terminal.unit.TerminalAction;

public interface StartOptionsAction extends TerminalAction<StartOptionsAction.Actionable> {
	interface Actionable {
		void onNewSniperTraining();

		void onNextSniperTrainingExistingProfile();

		void onResumeSavedTraining();

		void onViewSniperProfile();

		void onExit();
	}
}
