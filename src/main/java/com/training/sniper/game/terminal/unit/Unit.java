package com.training.sniper.game.terminal.unit;

import static java.util.stream.IntStream.rangeClosed;

public interface Unit {

	void print();

	default void rePrint() {
		clear();
		print();
	}

	default void clear() {
		rangeClosed(1, 5).forEach(value -> System.out.println());
	}

}
