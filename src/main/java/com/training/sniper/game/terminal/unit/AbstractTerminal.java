package com.training.sniper.game.terminal.unit;

public abstract class AbstractTerminal<T> implements Unit, TerminalAction<T> {

	protected T member;

	@Override
	public void setMember(T member) {
		this.member = member;

	}

}
