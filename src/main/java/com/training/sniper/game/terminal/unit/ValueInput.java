package com.training.sniper.game.terminal.unit;

import com.training.sniper.game.utils.OutputPrinter;
import com.training.sniper.game.utils.TerminalValueReader;

public class ValueInput implements Unit {

	private final String value;

	public ValueInput(final String value) {
		this.value = value;
	}

	@Override
	public void print() {
		OutputPrinter.printOutput(value);
	}

	public String getValue() {
		return TerminalValueReader.INSTANCE.readString();
	}

}
