package com.training.sniper.game.terminal.unit;

@SuppressWarnings("rawtypes")
public abstract class AbstractAnchor<T extends TerminalAction> implements Anchor {

	protected final T action;

	public AbstractAnchor(final T action) {
		this.action = action;
	}

	@Override
	public void display() {
		action.print();
	}
}
