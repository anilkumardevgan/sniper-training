package com.training.sniper.game.terminal.unit;

import java.util.function.Predicate;
import java.util.stream.Stream;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.utils.OptionsItemsLimits;
import com.training.sniper.game.utils.LambdaUtility;
import com.training.sniper.game.utils.OutputPrinter;
import com.training.sniper.game.utils.TerminalValueReader;

@SuppressWarnings("rawtypes")
public class StartOptions<T extends Enum> implements Unit {

	private final String optionsTitle;
	private final T[] optionsItems;
	private final OptionsItemsLimits acceptableOptionsItems;

	@SafeVarargs
	public StartOptions(final String optionsTitle, final T... optionsItems) {

		if (optionsItems.length == 0) {
			throw new IllegalArgumentException(Constants.OPTIONS_EMPTY);
		}
		this.optionsTitle = optionsTitle;
		this.optionsItems = optionsItems;
		this.acceptableOptionsItems = OptionsItemsLimits.of(1, optionsItems.length);
	}

	@Override
	public void print() {
		OutputPrinter.printOutput("\n" + optionsTitle);
		Stream.of(optionsItems).map(LambdaUtility.ENUMVAL_TO_STRINGVAL).forEach(System.out::println);
	}

	public T selectFromOptions() {
		printOptionsStatus(false);
		return optionsItems[readItemIndex()];
	}

	private int readItemIndex() {
		return TerminalValueReader.INSTANCE.readValidInput(optionsItemsRange(), rePrintWarn)
				- Constants.START_OPTIONS_ITEM_OFFSET;
	}

	private Predicate<String> optionsItemsRange() {
		return line -> acceptableOptionsItems.contains(Integer.parseInt(line));
	}

	private final Runnable rePrintWarn = () -> {
		rePrint();
		printOptionsStatus(true);
	};

	private void printOptionsStatus(final boolean invalidAction) {
		if (invalidAction) {
			OutputPrinter.printOutput(Constants.NOT_VALID_ACTION);
		}
		OutputPrinter.printOutput(Constants.SELECT_ACTION);
	}
}
