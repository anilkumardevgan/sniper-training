package com.training.sniper.game.terminal.unit;

import com.training.sniper.game.model.Sniper;

public interface AlertAction extends TerminalAction<AlertAction.Actionable> {
	void displayWinnerAlert(Sniper sniper);

	void displayPausedAlert();

	interface Actionable {

	}
}
