package com.training.sniper.game.terminal.action;

import java.util.Optional;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.model.Sniper;
import com.training.sniper.game.terminal.unit.AbstractAnchor;
import com.training.sniper.game.training.TrainingDesigner;
import com.training.sniper.game.training.TrainingDesignerFactory;
import com.training.sniper.game.utils.OutputPrinter;

public class StartOptionsAnchor extends AbstractAnchor<StartOptionsAction> implements StartOptionsAction.Actionable {

	private TrainingDesignerFactory sniperDesignFactory;

	public StartOptionsAnchor(StartOptionsAction action, TrainingDesignerFactory sniperDesignFactory) {
		super(action);
		this.action.setMember(this);
		this.sniperDesignFactory = sniperDesignFactory;
	}

	@Override
	public void onNewSniperTraining() {
		Optional<TrainingDesigner> optional = sniperDesignFactory.createSniper();
		if (optional.isPresent()) {
			optional.get().begin();
		} else {
			OutputPrinter.printOutput(Constants.CREATE_GAME_FAIL);
		}
		display();
	}

	@Override
	public void onNextSniperTrainingExistingProfile() {
		final Optional<TrainingDesigner> optional = sniperDesignFactory.designTrainingForSniper();
		if (optional.isPresent()) {
			optional.get().begin();
		} else {
			OutputPrinter.printOutput(Constants.SNIPER_PROFILE_DOES_NOT_EXIT);
		}
		display();
	}

	@Override
	public void onResumeSavedTraining() {
		final Optional<TrainingDesigner> optional = sniperDesignFactory.loadSniper();
		if (optional.isPresent()) {
			optional.get().begin();
		} else {
			OutputPrinter.printOutput(Constants.NO_SAVED_GAME);
		}
		display();

	}

	@Override
	public void onViewSniperProfile() {
		final Optional<Sniper> optional = sniperDesignFactory.fetchSniper();
		if (optional.isPresent()) {
			OutputPrinter.printOutput(optional.get().toString());
		} else {
			OutputPrinter.printOutput(Constants.SNIPER_PROFILE_DOES_NOT_EXIT);
		}
		display();
	}

	@Override
	public void onExit() {
		OutputPrinter.printOutput(Constants.ON_EXIT);
	}

}
