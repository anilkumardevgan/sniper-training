package com.training.sniper.game.terminal.action;

import com.training.sniper.game.constants.Constants;
import com.training.sniper.game.enums.Enums.StartOptionsItem;
import com.training.sniper.game.terminal.action.StartOptionsAction.Actionable;
import com.training.sniper.game.terminal.unit.AbstractTerminal;
import com.training.sniper.game.terminal.unit.StartOptions;
import com.training.sniper.game.utils.OutputPrinter;

public class StartOptionsTerminal extends AbstractTerminal<Actionable> implements StartOptionsAction {

	private StartOptions<StartOptionsItem> startOptions;

	public StartOptionsTerminal(final StartOptions<StartOptionsItem> options) {
		super();
		this.startOptions = options;
	}

	

	@Override
	public void print() {
		startOptions.print();
		switch (startOptions.selectFromOptions()) {
		case NEW_TRAINING:
			member.onNewSniperTraining();
			break;
		case NEXT_TRAINING:
			member.onNextSniperTrainingExistingProfile();
			break;
		case RESUME_SAVED_GAME:
			OutputPrinter.printOutput(Constants.LOAD_SNIPER);
			member.onResumeSavedTraining();
			break;
		case VIEW_SNIPER_PROFILE:
			member.onViewSniperProfile();
			break;
		case EXIT:
			member.onExit();
			break;
		}

	}

}
