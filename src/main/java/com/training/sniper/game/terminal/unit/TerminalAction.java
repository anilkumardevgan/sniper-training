package com.training.sniper.game.terminal.unit;

public interface TerminalAction<T> {

	void setMember(T member);

	void print();

}