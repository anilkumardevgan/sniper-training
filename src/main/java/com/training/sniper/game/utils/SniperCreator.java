package com.training.sniper.game.utils;

import java.util.ArrayList;
import java.util.List;

import com.training.sniper.game.enums.Enums.DamageCapability;
import com.training.sniper.game.enums.Enums.GunType;
import com.training.sniper.game.enums.Enums.Mode;
import com.training.sniper.game.enums.Enums.Opponent;
import com.training.sniper.game.model.Sniper;

public class SniperCreator {
	
	public static Sniper buildSniper() {
		Sniper sniper = new Sniper("Anil", Mode.CLASSIC, DamageCapability.FIFTY.getValue(), gunTypes());
        return sniper;
    }

    public static List<Sniper> buildOpponent() {
    	Sniper sniper = new Sniper(Opponent.ADELBERT_WALDRON.toString(),Mode.CLASSIC, DamageCapability.TWENTY.getValue(),gunTypes());
        final List<Sniper> list = new ArrayList<>();
        list.add(sniper);
        return list;
    }
    
    private static List<GunType> gunTypes(){
    	List<GunType> gunTypes = new ArrayList<>();
		gunTypes.add(GunType.BARRETT_M90);
		return gunTypes;
    }

}
