package com.training.sniper.game.utils;

import java.util.concurrent.ThreadLocalRandom;

public class RandomIntSelector {
	private RandomIntSelector() {
	}
	public static int getRandomIntFromRange(final int minVal, final int maxVal) {
		return ThreadLocalRandom.current().nextInt(minVal, maxVal);
	}

}
