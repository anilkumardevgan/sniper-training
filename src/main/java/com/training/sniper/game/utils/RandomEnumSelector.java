package com.training.sniper.game.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class RandomEnumSelector {

	private RandomEnumSelector(){
    }
	
    public static <T extends Enum<T>> T  randomEnum(final Set<T> options) {
        final List<T> list = new ArrayList<>(options);
        return list.get(RandomIntSelector.getRandomIntFromRange(0, list.size()));
    }
}
