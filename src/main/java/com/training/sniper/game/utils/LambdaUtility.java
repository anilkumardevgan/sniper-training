package com.training.sniper.game.utils;

import static java.util.Objects.nonNull;

import java.util.function.Function;
import java.util.function.Predicate;

import com.training.sniper.game.constants.Constants;
public class LambdaUtility {
	
   @SuppressWarnings("rawtypes")
   public static final Function<? super Enum, String> ENUMVAL_TO_STRINGVAL =
   enumVal -> enumVal.ordinal() + Constants.START_OPTIONS_ITEM_OFFSET + ". " + enumVal;

   public static final Predicate<String> VALUE_IS_NUM =
   val -> nonNull(val) && val.chars().allMatch(Character::isDigit);
                   
   public static final Predicate<String> VALUE_NOT_EMPTY =
   val -> nonNull(val) && !val.isEmpty() && val.chars().noneMatch(Character::isWhitespace);

   public static final Predicate<String> VALUE_POSITIVE = val -> Integer.parseInt(val) > 0;

   public static final Predicate<String> CORRECT_VALUE = VALUE_NOT_EMPTY.and(VALUE_IS_NUM).and(VALUE_POSITIVE);
   private LambdaUtility() {

   }
}
