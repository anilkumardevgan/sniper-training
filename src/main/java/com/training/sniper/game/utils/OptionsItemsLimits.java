package com.training.sniper.game.utils;

public final class OptionsItemsLimits {
	
	
	private final int minValue;
	private final int maxValue;
	
	private OptionsItemsLimits(final int min, final int max) {
        this.minValue = min;
        this.maxValue = max;
    }
	public static OptionsItemsLimits of(final int min, final int max) {
        if (min > max) {
            throw new IllegalStateException("Min value is greater than max. Min: " + min + ", Max: " + max);
        }
        return new OptionsItemsLimits(min, max);
    }
    public boolean contains(final int value) {
        return value >= minValue && value <= maxValue;
    }
    
    @Override
    public String toString() {
        return "ValueRange{min=" + minValue + ", max=" + maxValue + '}';
    }
}


