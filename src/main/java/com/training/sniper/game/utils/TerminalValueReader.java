package com.training.sniper.game.utils;


import static java.util.Objects.nonNull;

import java.util.Scanner;
import java.util.function.Predicate;

import com.training.sniper.game.constants.Constants;

public enum TerminalValueReader {

	INSTANCE;

	private final Scanner scanner;

	private TerminalValueReader() {
		scanner = new Scanner(System.in, Constants.UTF8);
	}

	public String readString() {
		return scanner.nextLine();
	}

	public int readValidInput(Predicate<String> playerCondition, Runnable onFailure) {
		final Predicate<String> retryCondition = LambdaUtility.CORRECT_VALUE.and(playerCondition).negate();
		String input = null;
		do {
			if (nonNull(input)) {
				onFailure.run();
			}
			input = readString().trim();
		} while (retryCondition.test(input));
		return Integer.parseInt(input);
	}
}
