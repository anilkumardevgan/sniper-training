package com.training.sniper.game.constants;

public class Constants {
	
	public static final int START_OPTIONS_ITEM_OFFSET = 1;
	public static final String WELCOME = "Welcome to Sniper Training Game!!";
    public static final String USER_SNIPER = "sniper.txt";
    public static final String OPPONENT_SNIPER = "opponent.txt";
    public static final String RES_FILE_PATH = "resource.path";
    public static final String NOT_VALID_ACTION="Not a valid Action. Please, type again.";
    public static final String SELECT_ACTION= "Select action number:";
    public static final String OPTIONS_EMPTY= "No items Present in Options.";
    public static final String LOAD_SNIPER= "\nLets load the previously saved training and player's profile.";
    public static final String ON_EXIT="Thank you for training with us. Hope you have upgraded your sniper skills.";
    public static final String SNIPER_PROFILE_DOES_NOT_EXIT="Profile does not exist. Please, try making a new Sniper.";
    public static final String NO_SAVED_GAME="No previous game saved.";
    public static final String CREATE_GAME_FAIL="Sorry Unable to create game now.";
    public static final String NO_ACTION_SPECIFIED="No Action specified for input";
    public static final String METHOD_NOT_SUPPORTED="This method is not supported";
    public static final String TRAINING_PAUSED="Training has been paused.";
    public static final String NAME_REQUIRED="Name is Required";
    public static final String GUN_TYPE_REQUIRED="GunType is Required";
    public static final String MODE_REQUIRED="Mode is Required";
    public static final String DC_REQUIRED="Damage Capability is Required";
    public static final String CREATE_SNIPER_OPTIONS="Create Sniper Options";
    public static final String SNIPER_NAME="Sniper name:";
    public static final String SNIPER_GUN="Choose Gun for Sniper :";
    public static final String SNIPER_MODE="Choose mode :";
    public static final String SNIPER_DC="Choose Max damage capability of Sniper :";
    public static final String SNIPER_ACTION="Choose player's action:";
    public static final String TRAINING_WINNER="This sniper training game is won by: ";
    public static final String UTF8="UTF-8";
    public static final String TEST_SNIPER="TestSniper";
    private Constants() {
        
    }
}
