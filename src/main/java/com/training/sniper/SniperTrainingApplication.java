package com.training.sniper;

import com.training.sniper.game.runner.SniperTrainingRunner;

public class SniperTrainingApplication 
{
    public static void main( String[] args )
    {
    	SniperTrainingRunner.start();
    }
}
